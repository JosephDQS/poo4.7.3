
/**
 * Décrivez votre classe fichier ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Fichier extends File
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private int taille;

    /**
     * Constructeur d'objets de classe fichier
     */
    public Fichier(String nom,int taille)
    {
       this.SetTaille(taille);
       this.SetNom(nom);
    }
   /**
     * Méthode pour set le nom du File
     *
     * @param  nom   le nom que l'on veux donner à notre objet File
     * @return     pas de retour
     */
    @Override
    public void SetNom(String nom) 
    {
        this.nom = nom;
    }

    /**
     * Méthode pour set la taille du Fichier en octet
     *
     * @param  taille   la taille que l'on veux donner à notre objet Fichier
     * @return     pas de retour
     */
    public void SetTaille(int taille)
    {
        this.taille = taille;
    }
    /**
     * Méthode pour get le nom du Fichier en octet
     *
     * @param  aucun
     * @return  nom de l'objet
     */
    @Override
    public String GetNom()
    {
        return this.nom;
    }
    /**
     * Méthode pour get la taille du Fichier en octet
     *
     * @param  aucun
     * @return  taille de l'objet
     */
    @Override
    public int GetTaille()
    {
        return this.taille;
    }
    
    /**
     * Méthode pour get le pere du Fichier
     *
     * @param  aucun
     * @return  taille de l'objet
     */
    public Repertoire GetPere()
    {
        return this.pere;
    }
    
    /**
     * Méthode pour set le pere du Fichier
     *
     * @param  taille   la taille que l'on veux donner à notre objet Fichier
     * @return     pas de retour
     */
    public void SetPere(Repertoire pere)
    {
        this.pere = pere;
    }
    
    
}
