import java.util.ArrayList;
/**
 * Décrivez votre classe repertoire ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Repertoire extends File
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private String nom;
    private ArrayList<File> liste;

    /**
     * Constructeur d'objets de classe repertoire
     */
    public Repertoire(String nom)
    {
        this.SetNom(nom);
        this.liste = new ArrayList<File>();
    }

    /**
     * Méthode pour set le nom du File
     *
     * @param  nom   le nom que l'on veux donner à notre objet File
     * @return     pas de retour
     */
    @Override
    public void SetNom(String nom) 
    {
        this.nom = nom;
    }
    
    /**
     * Méthode pour get le nom du Fichier en octet
     *
     * @param  aucun
     * @return  nom de l'objet
     */
    @Override
    public String GetNom()
    {
        return this.nom;
    }
    /**
     * Méthode pour get la taille du Repertoire en octet
     * 
     *
     * @param  aucun
     * @return  taille de l'objet 
     *
     */
    @Override
    public int GetTaille()
    {
      int len = 0;
      for(int i =0;i< this.liste.size();i++)
         {
          len = len + this.liste.get(i).GetTaille();
        }
        return len;
    }
   
    /**
     * Méthode pour vérifier qu'un fichier n'est pas descendant de lui meme
     * 
     *
     * @param  file un fichier
     * @return  true si file est descendant de lui meme, false sinon 
     *
     */
    
    private boolean FileEstDescendant(File file){
        pere = this.pere;
        File tmpFile = file;
        while(pere != null){
            if (pere == file){
                return true;
            }
            tmpFile = tmpFile.pere;
            pere = tmpFile.pere;
        }
        return false;
    }
    
    public void Ajout(File file)
    {
        if (this.FileEstDescendant(file)){
            return;
        }
        file.pere = this;
        if (this != file){
            this.liste.add(file);
        }
    }
    
}
