
/**
 * Décrivez votre classe file ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public abstract class File
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    protected String nom;
    protected Repertoire pere;
   
    public abstract void SetNom(String nom);
    public abstract String GetNom();
    public abstract int GetTaille();
}
